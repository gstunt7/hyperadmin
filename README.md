HyperAdmin is a script for administrators that is very useful. It simplifies your work and makes it much faster. 
The script offers you a lot of interesting and useful features that are not available in SA-MP. 
The script was developed by me specifically for the UIF and will never steal your passwords. Use it or not - only your choice.